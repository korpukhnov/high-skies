Реализация тестового задания по формированию отчёта о посадках воздушных судов
----------

Для локального развёртывания требуется:
- выполнить скрипты high_skies_db_structure.sql и high_skies_test_data.sql
- выполнить команду mvn package
- запустить HighSkiesApplication как консольное приложение