package com.highskies.persistence.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class LandingReportRowEntity {

    private int airplaneId;
    private String airplaneName;
    private int landingsCount;

    @Id
    @Column(name = "airplane_id", nullable = false)
    public int getAirplaneId() {
        return airplaneId;
    }

    public void setAirplaneId(int airplaneId) {
        this.airplaneId = airplaneId;
    }

    public String getAirplaneName() {
        return airplaneName;
    }

    public void setAirplaneName(String airplaneName) {
        this.airplaneName = airplaneName;
    }

    public int getLandingsCount() {
        return landingsCount;
    }

    public void setLandingsCount(int landingsCount) {
        this.landingsCount = landingsCount;
    }
}
