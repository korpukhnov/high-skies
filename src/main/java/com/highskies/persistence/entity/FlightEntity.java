package com.highskies.persistence.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.sql.Time;
import java.util.Objects;

@Entity
@Table(name = "flight", schema = "public", catalog = "high_skies_db")
public class FlightEntity {
    private int id;
    private Time flightTime;
    private int dayOfWeek;
    private AirplaneEntity airplaneByAirplaneId;
    private AirportEntity airportByDepartureAirportId;
    private AirportEntity airportByDestinationAirportId;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "flight_time", nullable = false)
    public Time getFlightTime() {
        return flightTime;
    }

    public void setFlightTime(Time flightTime) {
        this.flightTime = flightTime;
    }

    @Basic
    @Column(name = "day_of_week", nullable = false)
    public int getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(int dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FlightEntity that = (FlightEntity) o;
        return id == that.id &&
                dayOfWeek == that.dayOfWeek &&
                Objects.equals(flightTime, that.flightTime);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, flightTime, dayOfWeek);
    }

    @ManyToOne
    @JoinColumn(name = "airplane_id", referencedColumnName = "id", nullable = false)
    public AirplaneEntity getAirplaneByAirplaneId() {
        return airplaneByAirplaneId;
    }

    public void setAirplaneByAirplaneId(AirplaneEntity airplaneByAirplaneId) {
        this.airplaneByAirplaneId = airplaneByAirplaneId;
    }

    @ManyToOne
    @JoinColumn(name = "departure_airport_id", referencedColumnName = "id", nullable = false)
    public AirportEntity getAirportByDepartureAirportId() {
        return airportByDepartureAirportId;
    }

    public void setAirportByDepartureAirportId(AirportEntity airportByDepartureAirportId) {
        this.airportByDepartureAirportId = airportByDepartureAirportId;
    }

    @ManyToOne
    @JoinColumn(name = "destination_airport_id", referencedColumnName = "id", nullable = false)
    public AirportEntity getAirportByDestinationAirportId() {
        return airportByDestinationAirportId;
    }

    public void setAirportByDestinationAirportId(AirportEntity airportByDestinationAirportId) {
        this.airportByDestinationAirportId = airportByDestinationAirportId;
    }
}
