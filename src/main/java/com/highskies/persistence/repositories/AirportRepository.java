package com.highskies.persistence.repositories;

import com.highskies.persistence.entity.AirportEntity;
import org.springframework.data.repository.Repository;

import java.util.List;

public interface AirportRepository extends Repository<AirportEntity, Long> {
    List<AirportEntity> findAll();
}
