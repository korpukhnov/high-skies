package com.highskies.persistence.repositories;

import com.highskies.persistence.entity.LandingReportRowEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface LandingReportRepository extends Repository<LandingReportRowEntity, Long> {

    @Query(value = "select * from get_landing_report(:requested_airport_id)", nativeQuery = true)
    List<LandingReportRowEntity> findAllBy(@Param("requested_airport_id") Integer requestedAirportId);
}
