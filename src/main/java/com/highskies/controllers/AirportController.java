package com.highskies.controllers;

import com.highskies.persistence.entity.AirportEntity;
import com.highskies.persistence.repositories.AirportRepository;
import com.highskies.services.AirportsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class AirportController {

    private AirportsService airportsService;

    @Autowired
    public AirportController(AirportsService airportsService) {
        this.airportsService = airportsService;
    }

    @GetMapping("/api/airports")
    @ResponseBody
    public List<AirportEntity> getAirportsList() {
        return airportsService.getAirportsList();
    }


}