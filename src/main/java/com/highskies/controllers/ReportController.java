package com.highskies.controllers;

import com.highskies.persistence.entity.LandingReportRowEntity;
import com.highskies.services.LandingReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class ReportController {

    private LandingReportService landingReportService;

    @Autowired
    public ReportController(LandingReportService landingReportService) {
        this.landingReportService = landingReportService;
    }

    @GetMapping("/api/lading_report")
    @ResponseBody
    public List<LandingReportRowEntity> getAirportsList(@RequestParam Integer airportId) {
        return landingReportService.getLandingReport(airportId);
    }
}
