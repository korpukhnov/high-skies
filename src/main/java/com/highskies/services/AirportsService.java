package com.highskies.services;

import com.highskies.persistence.entity.AirportEntity;
import com.highskies.persistence.repositories.AirportRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AirportsService {

    private AirportRepository airportRepository;

    public AirportsService(AirportRepository airportRepository) {
        this.airportRepository = airportRepository;
    }

    public List<AirportEntity> getAirportsList() {

        return (List<AirportEntity>) airportRepository.findAll();

    }

}



