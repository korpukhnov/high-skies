package com.highskies.services;

import com.highskies.persistence.entity.LandingReportRowEntity;
import com.highskies.persistence.repositories.LandingReportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LandingReportService {

    private LandingReportRepository landingReportRepository;

    @Autowired
    public LandingReportService(LandingReportRepository landingReportRepository) {
        this.landingReportRepository = landingReportRepository;
    }

    public List<LandingReportRowEntity> getLandingReport(int airportId) {
        return landingReportRepository.findAllBy(airportId);
    }
}
