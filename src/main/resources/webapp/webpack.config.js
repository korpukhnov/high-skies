var path = require('path');

module.exports = {
    entry: './src/app.js',
    output: {
        path: __dirname + '/../static/',
        filename: 'bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['react', 'es2015', 'stage-2'],
                        plugins: ["transform-decorators-legacy"]
                    }
                }
            }
        ]
    },
    devServer: {
        contentBase: __dirname + '/../static/',
        compress: true,
        port: 9000,
        proxy: {
            '/api': 'http://localhost:8080'
        }
    }
};