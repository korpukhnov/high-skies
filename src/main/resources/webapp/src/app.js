import React from 'react';
import ReactDOM from 'react-dom';
import Button from '@material-ui/core/Button';
import Table from "@material-ui/core/es/Table/Table";
import Select from "@material-ui/core/es/Select/Select";
import MenuItem from "@material-ui/core/es/MenuItem/MenuItem";
import store from "./redux/store";
import {Provider} from "react-redux";
import LandingReportPage from "./pages/LandingReportPage";

ReactDOM.render(
    <Provider store={store}>
        <LandingReportPage/>
    </Provider>,
    document.querySelector('#app')
);