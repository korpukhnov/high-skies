import React from 'react';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {fetchReportData} from "../redux/action_creators/reportDataActions";
import {fetchAirportsList, selectAirport} from "../redux/action_creators/airportActoins";
import CircularProgress from "@material-ui/core/es/CircularProgress/CircularProgress";
import AirportSelector from "../components/AirportSelector";
import LandingReportTable from "../components/LandingReportTable";
import Grid from "@material-ui/core/es/Grid/Grid";
import Typography from "@material-ui/core/es/Typography/Typography";
import Icon from "@material-ui/core/es/Icon/Icon";

@connect((state) => {
        return {
            airport: state.airport,
            report: state.report.reportRows,
            fetchingReport: state.report.fetching,
        };
    },
    (dispatch) => {
        return {
            fetchReportData: bindActionCreators(fetchReportData, dispatch),
            selectAirport: bindActionCreators(selectAirport, dispatch),
            fetchAirportsList: bindActionCreators(fetchAirportsList, dispatch),
        }
    })
export default class LandingReportPage extends React.Component {

    componentWillMount() {
        this.props.fetchAirportsList();
    }

    render() {
        return (
            <Grid container spacing={16}>
                <Grid item xs={12}>

                    <Typography variant="display2" style={{textTransform: 'uppercase', marginBottom: '62px'}}>
                        <Icon style={{ fontSize: 48 }}>flight_takeoff</Icon> Авиакомпания "Высокое небо"
                    </Typography>

                    <Typography variant="display1" gutterBottom align="center">
                        Отчёт по посадкам воздушных судов
                    </Typography>
                    <AirportSelector airportsList={this.props.airport.airportsList}
                                     selectedAirportId={this.props.airport.selectedAirportId}
                                     onAirportSelected={(airport) => this.props.selectAirport(airport)}
                    />
                </Grid>

                <Grid item xs={12}>
                    <LandingReportTable reportRows={this.props.report}/>
                    {this.props.fetchingReport &&
                    <div style={{textAlign: 'center', marginTop: '16px'}}>
                        <CircularProgress size={60}/>
                    </div>}
                </Grid>
            </Grid>
        );
    }
}