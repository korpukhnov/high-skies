import React from 'react';
import Table from "@material-ui/core/es/Table/Table";
import TableHead from "@material-ui/core/es/TableHead/TableHead";
import TableRow from "@material-ui/core/es/TableRow/TableRow";
import TableCell from "@material-ui/core/es/TableCell/TableCell";
import TableBody from "@material-ui/core/es/TableBody/TableBody";
import PropTypes from "prop-types";

class LandingReportTable extends React.Component {

    render() {
        const renderReportRows = (data) => {
            return data.map(n => {
                return (
                    <TableRow key={n.id}>
                        <TableCell numeric>{n.airplaneId}</TableCell>
                        <TableCell component="th" scope="row">{n.airplaneName}</TableCell>
                        <TableCell numeric>{n.landingsCount}</TableCell>
                    </TableRow>
                );
            })
        };

        return (
            <Table>
                <TableHead>
                    <TableRow>
                        <TableCell numeric>ID воздушного судна</TableCell>
                        <TableCell>Наименование воздушного судна</TableCell>
                        <TableCell numeric>Количество посадок в неделю</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>{renderReportRows(this.props.reportRows)}</TableBody>
            </Table>
        );
    }
}


LandingReportTable.propTypes = {
    reportRows: PropTypes.array,
};

export default LandingReportTable;