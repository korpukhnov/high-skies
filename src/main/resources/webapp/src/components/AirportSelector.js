import React from 'react';
import Select from "@material-ui/core/es/Select/Select";
import MenuItem from "@material-ui/core/es/MenuItem/MenuItem";
import Input from "@material-ui/core/es/Input/Input";
import InputLabel from "@material-ui/core/es/InputLabel/InputLabel";
import FormControl from "@material-ui/core/es/FormControl/FormControl";
import PropTypes from 'prop-types';
import Card from "@material-ui/core/es/Card/Card";
import CardContent from "@material-ui/core/es/CardContent/CardContent";

class AirportSelector extends React.Component {

    render() {
        const {airportsList, selectedAirportId, onAirportSelected} = this.props;
        const options = () => airportsList.map(({id, name}) =>
            <MenuItem value={id}>{name}</MenuItem>
        );
        return (
            <Card style={{margin: '10px'}}>
                <CardContent>
                    <FormControl>
                        <InputLabel htmlFor="airport-selector">Выбраннный аэропорт</InputLabel>
                        <Select
                            value={selectedAirportId}
                            onChange={(event) => onAirportSelected(event.target.value)}
                            input={<Input name="airport" id="airport-selector" style={{width: '300px'}}/>}
                            autoWidth={true}
                        >{options()}</Select>
                    </FormControl>
                </CardContent>
            </Card>
        );
    }
}

AirportSelector.propTypes = {
    airportsList: PropTypes.array,
    selectedAirportId: PropTypes.number,
    onAirportSelected: PropTypes.func,
};

export default AirportSelector;