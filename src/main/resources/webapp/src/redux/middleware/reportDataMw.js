import {REPORT_DATA_FETCHING, SELECT_AIRPORT} from "../../constants/apiTypes";
import {fetchReportData} from "../action_creators/reportDataActions";

export const reportDataMw = ({getState, dispatch}) => next => action => {
    next(action);

    if (action.type === SELECT_AIRPORT) {
        dispatch({type: REPORT_DATA_FETCHING});
        dispatch(fetchReportData(action.payload));
    }

};