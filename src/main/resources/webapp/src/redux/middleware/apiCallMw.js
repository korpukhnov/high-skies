import {API_CALL, API_CALL_FAILED, API_CALL_SUCCESS} from "../../constants/apiTypes";

export const apiCallMw = ({getState, dispatch}) => next => action => {
    console.log("Middleware triggered:", action);
    next(action);

    if (action.type === API_CALL) {
        fetch(action.url)
            .then(response => {
                if (response.ok) {
                    return response.json()
                } else {
                    dispatch({
                        type: API_CALL_FAILED,
                        url: action.url,
                        message: 'Ошибка связи с сервером ' + response.status
                    })
                }
            })
            .then(data => action.onSuccess(data, dispatch, getState));
    }

};