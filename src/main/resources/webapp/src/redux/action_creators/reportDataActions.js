import {API_CALL, REPORT_DATA_FETCHED} from "../../constants/apiTypes";
import {REPORT_DATA_URL} from "../../constants/urls";

export const fetchReportData = (airportId) => {
    return {
        type: API_CALL,
        url: REPORT_DATA_URL + '?airportId=' + airportId,
        onSuccess: (data, dispatch) => dispatch(saveReport(data))
    }
};

export const saveReport = (reportData) => {
    return {
        type: REPORT_DATA_FETCHED,
        payload: reportData
    }
};