import {AIRPORTS_FETCHED, API_CALL, SELECT_AIRPORT} from "../../constants/apiTypes";
import {AIRPORTS_URL} from "../../constants/urls";

export const fetchAirportsList = () => {
    return {
        type: API_CALL,
        url: AIRPORTS_URL,
        onSuccess: (data, dispatch) => dispatch(saveAirports(data))
    }
};

export const saveAirports = (airports) => {
    return {
        type: AIRPORTS_FETCHED,
        payload: airports
    }
};

export const selectAirport = (airportId) => {
    return {
        type: SELECT_AIRPORT,
        payload: airportId
    }
};