import createStore from "redux/src/createStore";
import {applyMiddleware, combineReducers, compose} from "redux";
import airportReducer from "./reducres/airportReducer";
import {apiCallMw} from "./middleware/apiCallMw";
import {reportDataMw} from "./middleware/reportDataMw";
import landingReportReducer from "./reducres/landingReportReducer";

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
export default createStore(
    combineReducers({
        airport: airportReducer,
        report: landingReportReducer,
    }),
    composeEnhancers(
        applyMiddleware(apiCallMw, reportDataMw)
    )
);
