import {REPORT_DATA_FETCHED, REPORT_DATA_FETCHING} from "../../constants/apiTypes";

const initialState = {
    reportRows: [],
    fetching: false,
    fetched: true,
};


export default function landingReportReducer(state = initialState, action) {
    switch (action.type) {
        case REPORT_DATA_FETCHED:
            return {
                ...state,
                reportRows: action.payload,
                fetching: false,
                fetched: true,
            };
        case REPORT_DATA_FETCHING:
            return {
                ...state,
                reportRows: [],
                fetching: true,
                fetched: false
            }
    }
    return state;
}