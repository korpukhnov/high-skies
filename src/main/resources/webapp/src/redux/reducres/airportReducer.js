import {AIRPORTS_FETCHED, SELECT_AIRPORT} from "../../constants/apiTypes";

const initialState = {
    airportsList: [],
    selectedAirportId: 0,
    fetching: false,
    fetched: true,
};


export default function airportReducer(state = initialState, action) {
    switch (action.type) {
        case AIRPORTS_FETCHED:
            return {
                ...state,
                airportsList: action.payload,
                fetching: false,
                fetched: true,
            };
        case SELECT_AIRPORT:
            return {
                ...state,
                selectedAirportId: action.payload,
            };
    }
    return state;
}