﻿DROP DATABASE high_skies_db;
CREATE DATABASE high_skies_db
  WITH OWNER = postgres
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       LC_COLLATE = 'Russian_Russia.1251'
       LC_CTYPE = 'Russian_Russia.1251'
       CONNECTION LIMIT = -1;

-- Содержит информацию о самолёте
DROP TABLE public.airplane CASCADE;
CREATE TABLE public.airplane (
  id serial NOT NULL,
  name varchar,
  CONSTRAINT airplane_pkey PRIMARY KEY (id)
);
ALTER TABLE public.airplane OWNER TO postgres;

-- Содержит информацию о городе расположения аэропорта
DROP TABLE public.city CASCADE;
CREATE TABLE public.city(
  id serial NOT NULL,
  name varchar NOT NULL,
  CONSTRAINT city_pkey PRIMARY KEY (id)
);
ALTER TABLE public.city OWNER TO postgres;

-- Содержит информацию об аэропорте
DROP TABLE public.airport CASCADE;
CREATE TABLE public.airport(
  id serial NOT NULL,
  name varchar NOT NULL,
  city_id bigint NOT NULL REFERENCES public.city(id),
  CONSTRAINT airport_pkey PRIMARY KEY (id)
);
ALTER TABLE public.airport OWNER TO postgres;

-- Содержит информацию о маршруте
DROP TABLE public.route CASCADE;
CREATE TABLE public.route(
  id serial NOT NULL,
  name varchar NOT NULL,
  code varchar NOT NULL,
  CONSTRAINT route_pkey PRIMARY KEY (id)
);
ALTER TABLE public.route OWNER TO postgres;

-- Содержит информацию о конкретном перелёте в составе маршрута 
DROP TABLE public.flight;
CREATE TABLE public.flight(
  id serial NOT NULL,
  route_id bigint NOT NULL REFERENCES public.route(id),
  airplane_id bigint NOT NULL REFERENCES public.airplane(id),
  departure_airport_id bigint NOT NULL REFERENCES public.airport(id),
  destination_airport_id bigint NOT NULL REFERENCES public.airport(id),
  flight_time time NOT NULL,
  -- Содержит день недели выполнения перелёта.
  day_of_week int NOT NULL,
  CONSTRAINT flight_pkey PRIMARY KEY (id)
);
ALTER TABLE public.flight OWNER TO postgres;
