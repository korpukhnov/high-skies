﻿DROP TYPE flight_report CASCADE;
CREATE TYPE flight_report AS (
  airplane_id int,
  airplane_name varchar,
  landings_count int
);

CREATE OR REPLACE FUNCTION get_landing_report(requested_airport_id int) RETURNS SETOF flight_report AS
$$
DECLARE
  _result flight_report;
BEGIN
  FOR _result.airplane_id, _result.airplane_name, _result.landings_count IN 
  (
    SELECT
    airplane.id as airplane_id,
    airplane.name as airplane_name,
    count(flight.id) as landings_count

    FROM flight, route, airplane

    WHERE
    flight.route_id = route.id
    AND flight.airplane_id = airplane.id
    AND flight.destination_airport_id = requested_airport_id

    GROUP BY airplane.id
  )
  LOOP
    RETURN NEXT _result;
  END LOOP;
  RETURN;
END
$$ LANGUAGE plpgsql;

select * from get_landing_report(4);